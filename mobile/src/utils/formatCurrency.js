const formatCurrency = ({
  language = 'pt-BR',
  style = 'currency',
  currency = 'BRL',
  value,
}) => Intl.NumberFormat(language, { style, currency }).format(value);

export default formatCurrency;
